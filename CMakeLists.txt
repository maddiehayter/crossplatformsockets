# CMakeLists files in this project can
# refer to the root source directory of the project as ${HELLO_SOURCE_DIR} and
# to the root binary directory of the project as ${HELLO_BINARY_DIR}.
cmake_minimum_required(VERSION 3.1)

set(PROJECT_NAME_STR NetworkGame)
project(${PROJECT_NAME_STR} C CXX)

# VS Code gives a warning about this, good artilcle here https://stackoverflow.com/questions/24460486/cmake-build-type-is-not-being-used-in-cmakelists-txt
# cmake -H. -B_builds -DCMAKE_CONFIGURATION_TYPES="Debug;Release" "-GUnix Makefiles" ../
# cmake --build _builds --config Debug
# Gets rid of the error, VS Studio is multi-config and should generate all configurations anyway. 
message("Generated with config types: ${CMAKE_CONFIGURATION_TYPES}")

#from
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# For older CMake versions 
#from - https://stackoverflow.com/questions/44478492/cmake-failed-to-run-msbuild-command-msbuild-exe
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
# set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")


# Should work for versions 3.1 on!
# set (CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_STANDARD 14)

find_package(Threads REQUIRED)

if(CMAKE_COMPILER_IS_GNUCXX)
  # New CMakeOnly
  # add_definitions(-Wall -ansi -Wno-deprecated -pthread)
  # Note: -ansi reinforces C90, will override c++11
  set(GNU_EXTRA_STUFF "-Wall -Wno-deprecated -pthread")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GNU_EXTRA_STUFF}")
endif()

if(MSVC)
    #vc 2012 fix for vararg templates
    set(MSVC_COMPILER_DEFS "-D_VARIADIC_MAX=10")
endif()

#-------------------
# set common include folder for module
#-------------------

# message(STATUS "Source dir:" ${PROJECT_SOURCE_DIR})
set(COMMON_INCLUDES ${PROJECT_SOURCE_DIR}/src/strings ${PROJECT_SOURCE_DIR}/src/networking)
set(EXT_PROJECTS_DIR ${PROJECT_SOURCE_DIR}/ext)

# Recurse into the "maths" etc subdirectories. This does not actually
# cause another cmake executable to run. The same process will walk through
# the project's entire directory structure.

# add_subdirectory (maths)
add_subdirectory (src/strings)
add_subdirectory (src/networking)
add_subdirectory (src/game)
add_subdirectory (src/gtest)





